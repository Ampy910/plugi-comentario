<?php
/*
Plugin Name: Call Clement
Plugin URI: http://wordpress.org/plugins/hello-dolly/
Description: funciona parapasar datos 
Author: Matt Mullenweg
Version: 0.0.1
Author URI: http://ma.tt/

*/

function shortcode_call($atts){
    ob_start();
    
    $args = array(
        'numberposts' => 6, 
        'posts_per_page' => '6',
    );

    $the_query = new WP_Query( $args );
    if($the_query->have_posts() ) : 
        
        while ( $the_query->have_posts() ) : 
            $the_query->the_post(); 
            $id = get_the_ID();
            ?><div class="contenedor">
            <div class="card-custom" >
                <?=get_the_excerpt();?><br><div class="conten">
                <div class="name">
                    
                <?=get_the_author_meta('display_name')?>
                <?=get_the_author_meta('user_description')?>
                
                <?php
                    $user_id =get_the_author_meta('ID');
                    $url_img = get_avatar_url($user_id);
                ?></div>
                
                    <img class="imagenn" src="<?=$url_img;?>" alt="">
                
            </div>
        </div>
    </div>
            <?php
        endwhile; 
        wp_reset_postdata(); 
    endif;
    return ob_get_clean();
}

add_shortcode("shortcodecall","shortcode_call");

add_action( 'wp_enqueue_scripts', 'shortcode_call_css' );
function shortcode_call_css() {
 
    wp_register_style( 'shortcode_call_css', plugin_dir_url(__FILE__).'/style.css?v=6' );
    wp_enqueue_style( 'shortcode_call_css' );
 
}
?>